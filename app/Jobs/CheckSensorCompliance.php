<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Sensor;
use App\ComplianceStatus;
use App\Helpers\IssueHelper;
use Carbon\Carbon;

class CheckSensorCompliance implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $sensor = null;
    private $log = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($log)
    {
        $this->log = $log;
        $this->sensor = Sensor::with('location')->findOrFail($log->sensor_id);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // return early if the sensor is inactive as we
        // don't care about alarms on this right now.
        if ($this->sensor->status == 'inactive') {
            return;
        }

        $alarmcollection = collect([$this->sensor->genericAlarms, $this->sensor->customAlarms]);
        // iterate over both generics and customs
        foreach($alarmcollection as $alarms) {
            // for each alarm test compliance
            foreach($alarms as $alarm) {
                $compliance = $this->getOrCreateStatus($alarm);
                if (is_null($compliance->id)) {
                    $compliance->status = "1";
                }
                if ($this->noncompliant($alarm)) {
                    switch ($compliance->status) {
                    case "1": // (OK)
                    {
                        /**
                         * Progress status to warning state as compliance failed
                         */
                        $compliance->status = "2";
                        $compliance->status_changed = Carbon::now();
                        break;
                    }
                    case "2": // (WARNING)
                    {
                        /**
                         * If the time since status changed has passed the alarm delay
                         * upgrade to alarm status and create a new issue event
                         */
                        $diff = Carbon::parse($compliance->status_changed)->diffInSeconds(Carbon::now());
                        if ($diff > $alarm->delay) {
                            $issue = IssueHelper::alarm($this->sensor, $this->log, $compliance, $alarm);
                            $compliance->issue_id = $issue->id;
                            $compliance->status = "3";
                            $compliance->status_changed = Carbon::now();
                        }
                        break;
                    }
                    case "3": // (ALARM)
                    {
                        /**
                         * If the time since the status went into alarm has gone past the 
                         * realarm delay and realarm is enabled. Update the assigned issue
                         * with a realarm message
                         */
                        $diff = Carbon::parse($compliance->status_changed)->diffInSeconds(Carbon::now());
                        if ($alarm->realarm && $diff > $alarm->realarm_delay) {

                            $issue = IssueHelper::realarm($this->sensor, $compliance, $alarm);

                            /**
                             * Update the status_changed so that the realarm will be triggered
                             * again after the same realarm_delay
                             */
                            $compliance->status_changed = Carbon::now();
                        }
                        break;
                    }
                    }
                } else {
                    /**
                     * If Compliant resolve any active issues for this alarm
                     * and set compliance status back to 1 (OK)
                     */
                    if ($compliance->status == "3") {
                        IssueHelper::solved($compliance->issue_id);
                        $compliance->issue_id = null;
                    }
                    $compliance->status = "1";
                    $compliance->status_changed = Carbon::now();
                }
                $compliance->save();
            }
        }
    }

    private function getOrCreateStatus($alarm)
    {
        return ComplianceStatus::firstOrNew([
            'sensor_id' => $this->sensor->id,
            'trigger' => $alarm->tag,
        ]);
    }

    private function noncompliant($alarm)
    {
        switch ($alarm->type) {
        case "greater":
        {
            return $this->log->min > $alarm->value;
        }
        case "less":
        {
            return $this->log->max < $alarm->value;
        }
        }
    }
}
