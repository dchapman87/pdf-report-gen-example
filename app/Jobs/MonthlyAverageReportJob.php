<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Carbon\Carbon;
use App\Customer;
use App\Reports\ReportPeriod;

class MonthlyAverageReportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function tags()
    {
        return ['report', 'job'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $yesterday = Carbon::now()->subDay(1)->toDateString();
        $currentPeriod = new ReportPeriod($yesterday);

        Customer::all()->each(function ($customer, $index) use ($currentPeriod) {
            $period = $customer->created_at;
            $count = 0;
            while ($period->startOfMonth() != $currentPeriod->start()) {
                GenMonthlyAverageReport::dispatch($customer, new ReportPeriod($period))->onQueue('reports');
                $period = $period->addMonth(1);
                ++$count;
            }
            GenMonthlyAverageReport::dispatch($customer, $currentPeriod)->onQueue('reports');
        });
    }
}
