<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Reports\MonthlyAverageReport;
use App\Reports\MonthlyAverageReportBuilder;
use App\Reports\ReportPeriod;
use App\Site;
use App\Customer;
use Carbon\Carbon;

class GenMonthlyAverageReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 600; // 10 minutes

    private $customer;
    private $period;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Customer $customer, ReportPeriod $period)
    {
        $this->customer = $customer;
        $this->period = $period;
    }

    public function tags()
    {
        return ['monthly average', 'reports', 'report'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sites = Site::where('customer_id', $this->customer->id)->get();
        if ($sites->isEmpty()) {
            return; // Nothing left todo.
        }

        // First build the full report
        $this->buildReport();

        foreach ($sites as $site) {
            // Now build per site report
            $this->buildReport($site);
        }
    }

    private function buildReport($site = null)
    {
        $monthly = new MonthlyAverageReport($this->customer, $site, $this->period);
        if ($monthly->exists() && $this->period->start() != Carbon::now()->startOfMonth()) {
            return;
        }
        $report = $monthly->build(new MonthlyAverageReportBuilder($this->customer, $site, $this->period));
        $monthly->save($report);
    }
}
