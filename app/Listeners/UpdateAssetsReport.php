<?php

namespace App\Listeners;

use App\Events\Event;
use App\Events\SensorCreated;
use App\Events\SensorUpdated;
use App\Events\SensorDeleted;
use App\Reports\AssetsReport;
use App\Reports\AssetsReportBuilder;
use App\Reports\Report;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\SiteCreated;
use App\Events\SiteDeleted;
use App\Events\DeviceCreated;
use App\Events\DeviceDeleted;
use App\Site;
use App\Customer;
use App\Events\DeviceUpdated;
use App\Events\SiteUpdated;

class UpdateAssetsReport implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(Event $event)
    {
        $customer = null;
        $site = null;

        if ($event instanceof SiteCreated || $event instanceof SiteUpdated || $event instanceof SiteDeleted) {
            $site = $event->site;
            $customer = $site->customer;
        } elseif ($event instanceof DeviceCreated || $event instanceof DeviceUpdated || $event instanceof DeviceDeleted) {
            $site = $event->device->site;
            $customer = $site->customer;
        } elseif ($event instanceof SensorCreated || $event instanceof SensorUpdated || $event instanceof SensorDeleted) {
            $sensor = $event->sensor;
            $site = $sensor->device->site;
            $customer = $site->customer;
        }
        // TODO: Outputs

        // First we gen the pdf that includes all sites
        $assets = new AssetsReport($customer);
        try {
            $report = $assets->build(new AssetsReportBuilder($customer));
        } catch (\ErrorException $th) {
            // do nothing
        }
        $assets->save($report);

        // And if we have a specific site update that report as well.
        if (!is_null($site)) {
            $assets = new AssetsReport($customer, $site);
            $report = $assets->build(new AssetsReportBuilder($customer, $site));
            $assets->save($report);
        }
    }
}
