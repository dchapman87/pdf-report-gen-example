<?php

namespace App\Listeners;

use App\Events\SensorLogCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\CheckSensorCompliance;
use Illuminate\Support\Facades\Log;

class SensorLogCreatedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  SensorLogCreated  $event
     * @return void
     */
    public function handle(SensorLogCreated $event)
    {
        Log::debug("SensorLogCreated dispatched");
        CheckSensorCompliance::dispatch($event->log);
    }
}
