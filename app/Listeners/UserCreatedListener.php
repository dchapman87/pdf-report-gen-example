<?php

namespace App\Listeners;

use App\Events\UserCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\CreatePasswordNotification;
use App\Notifications\AdminNewUser;
use Password;
use App\User;
use Notification;
use App;
use App\PasswordReset;

class UserCreatedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserCreated  $event
     * @return void
     */
    public function handle(UserCreated $event)
    {
        $user = $event->user;
        // We need to send a create password notification for new users
        if (App::environment('production')) {
            $passwordReset = PasswordReset::updateOrCreate([
                'email' => $user->email,
            ],[
                'email' => $user->email,
                'token' => str_random(60),
            ]);

            if ($passwordReset) {
                $user->notify(new CreatePasswordNotification($passwordReset));
            }

            $admin = User::listOnlyAdmin()->get();
            Notification::send($admin, new AdminNewUser($user));
        }
    }
}
