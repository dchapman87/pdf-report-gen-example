<?php

namespace App\Reports;

use Auth;
use App\Site;
use App\Customer;
use Carbon\Carbon;

class MonthlyAverageReport extends BaseReport
{
    protected $period;
    public function __construct(Customer $customer, Site $site = null, ReportPeriod $period = null)
    {
        parent::__construct($customer, $site);
        $custCondensed = preg_replace('/\s+/', '', $this->customer->name);
        $this->period = $period->start()->format('Y-m');
        $this->filename = "Report_Temperature_Monthly_Average_" . $custCondensed . ".pdf";
        $path = "reports/TemperatureMonthlyAverage";
        if (is_null($site)) {
            $this->filepath = sprintf("%s/%s/%s/%s", $path, $custCondensed, $this->period, $this->filename);
        } else {
            $siteCondensed = preg_replace('/\s+/', '', $this->site->name);
            $this->filepath = sprintf("%s/%s/%s/%s/%s", $path, $custCondensed, $siteCondensed, $this->period, $this->filename);
        }
    }
}
