<?php
namespace App\Reports;

use PDF;

class Report
{
    private $content;
    private $filename;

    public function __construct()
    {
    }

    public function content()
    {
        return $this->content;
    }

    public function setContent($data)
    {
        $this->content = $data;
    }

    public function filename()
    {
        return $this->filename;
    }

    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

    // public function pdf()
    // {
    //     PDF::SetTitle($this->title);
    //     PDF::SetCreator("RCM Networks Ltd");
    //     PDF::SetAuthor("RCM Networks Ltd");
    //     PDF::SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', '');
    //     PDF::AddPage();
    //     PDF::SetFont('helvetica', '', 9);
    //     PDF::SetAutoPageBreak(true, 10);
    //     PDF::SetTextColor(0, 0, 0);
    //     PDF::WriteHtml($this->content, true, false, true, false, '');
    //     PDF::LastPage();
    //     PDF::Output($this->filename, 'D');
    // }
}
