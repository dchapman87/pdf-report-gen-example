<?php
namespace App\Reports;

use App\Customer;

class NonCompliantReportBuilder implements ReportBuilder
{
    private $customer;
    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    public function isValid(): bool
    {
        return !is_null($this->customer);
    }

    public function build(string $filename): Report
    {
        $report = new Report();
        $report->setFilename($filename);
        PDF::reset();
        PDF::SetCreator(PDF_CREATOR);
        PDF::SetAuthor(PDF_AUTHOR);
        PDF::SetTitle('Non Compliance Report');
        PDF::SetSubject('');
        PDF::SetKeywords('');
        PDF::SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', '');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(PDF_MARGIN_HEADER);
        PDF::SetFooterMargin(PDF_MARGIN_FOOTER);
        PDF::SetFont('helvetica', '', 9);
        PDF::SetAutoPageBreak(true, 10);
        PDF::SetTextColor(0, 0, 0);
        $ncData = new NonCompliantReportData($this->customer);
        $ncData->data()->each(function ($data, $idx) {
            PDF::AddPage();

        });
        $ncData->cleanup();
        return $report;
    }


}