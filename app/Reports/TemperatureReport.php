<?php

namespace App\Reports;

use App\Customer;
use App\Site;
use Carbon\Carbon;

class TemperatureReport extends BaseReport
{
    protected $period;
    public function __construct(Customer $customer, Site $site = null, ReportPeriod $period = null)
    {
        parent::__construct($customer, $site);
        $custCondensed = preg_replace('/\s+/', '', $this->customer->name);
        $this->filename = "Report_2_" . $custCondensed . ".pdf";
        $this->period = $period->start()->format('Y-m');
        $path = "reports/Temperature";
        if (is_null($site)) {
            $this->filepath = sprintf("%s/%s/%s/%s", $path, $custCondensed, $this->period, $this->filename);
        } else {
            $siteCondensed = preg_replace('/\s+/', '', $this->site->name);
            $this->filepath = sprintf("%s/%s/%s/%s/%s", $path, $custCondensed, $siteCondensed, $this->period, $this->filename);
        }
    }
}
