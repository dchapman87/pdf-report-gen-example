<?php
namespace App\Reports;

use App\Customer;
use App\Site;
use App\Sensor;
use PDF;
use Carbon\Carbon;
use App\SensorTypeCustom;

class MonthlyAverageReportBuilder implements ReportBuilder
{
    private $site;
    private $customer;
    private $period;
    private $sites;
    private $columns;

    public function __construct(Customer $customer = null, Site $site = null, ReportPeriod $period = null)
    {
        if (!is_null($site)) {
            $this->site = $site;
            if (is_null($customer)) {
                $this->customer = $site->customer;
            }
        }

        if (!is_null($customer)) {
            $this->customer = $customer;
        }

        if (is_null($site)) {
            $this->sites = $customer->sites;
        } else {
            $this->sites = collect([$site]);
        }

        $this->period = $period;

        $this->columns = collect([
            [ 'header' => 'Sen ID',   'width' => 10, 'align' => 'C' ],
            [ 'header' => 'Name',     'width' => 30, 'align' => 'L' ],
            [ 'header' => 'Location', 'width' => 76, 'align' => 'L' ],
            [ 'header' => 'Type',     'width' => 26, 'align' => 'L' ],
            [ 'header' => 'Site',     'width' => 8,  'align' => 'C' ],
            [ 'header' => 'Min',      'width' => 11, 'align' => 'C' ],
            [ 'header' => 'Avg',      'width' => 11, 'align' => 'C' ],
            [ 'header' => 'Max',      'width' => 11, 'align' => 'C' ],
        ]);
    }

    public function isValid(): bool
    {
        return !is_null($this->customer);
    }

    public function build(string $filename) : Report
    {
        $report = new Report();
        $report->setFilename($filename);
        PDF::reset();
        PDF::SetCreator(PDF_CREATOR);
        PDF::SetAuthor(PDF_AUTHOR);
        PDF::SetTitle('Temperature Monthly Average Report');
        PDF::SetSubject('');
        PDF::SetKeywords('');
        // TODO: Figure out why the logo isn't being inserted???
        PDF::SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', '');
        // set margins
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(PDF_MARGIN_HEADER);
        PDF::SetFooterMargin(PDF_MARGIN_FOOTER);

        // set header and footer fonts
        // PDF::SetHeaderFont([PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN]);
        // PDF::SetFooterFont([PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA]);
        PDF::SetFont('helvetica', '', 9);
        PDF::SetAutoPageBreak(true, 15);
        PDF::Ln(5);
        PDF::SetTextColor(0, 0, 0);
        $customer = $this->customer;
        $period = $this->period;
        $this->sites->each(function ($site, $index) use ($customer, $period) {
            $notes = collect([]);
            PDF::AddPage();
            $data = $this->getSiteSensors($site);
            $page = view('reports.content.monthlyaverage', [
                "customer" => $this->customer,
                "sites" => $this->sites,
                "site" => $site,
                "period" => $this->period,
                "data" => $data,
            ])->render();
            PDF::WriteHtml($page, true, false, true, false, '');
            $last = $data->count() - 1;
            $data->each(function ($sensor, $idx) use ($site, $last, $notes) {
                $logs = $sensor->log()->where('time', '>=', $this->period->start())
                    ->where('time', '<=', $this->period->end())
                    ->get();
                if (!$logs->isEmpty()) {
                    $fill = $idx % 2 != 0;
                    $this->writeHeader();
                    PDF::SetFillColor(224, 235, 255);
                    PDF::SetFont('');
                    $location = $sensor->location ? $sensor->location->title : "";
                    $min = $logs->min('min');
                    $avg = $logs->avg('avg');
                    $max = $logs->max('max');
                    $alarm = Sensor::findNonCompliantAlarm($sensor, $min, $max);
                    $this->writeRowStart($sensor, $location, $site, $alarm, $fill);
                    $this->writeRowEnd($alarm, $min, $avg, $max, $fill);

                    PDF::Ln();
                    if ($idx == $last) {
                        PDF::Cell($this->columns->sum("width"), 0, '', 'T');
                    }

                    if (!is_null($alarm)) {
                        $notes->push([
                            "sensor" => $sensor,
                            "location" => $location,
                            "alarm" => $alarm,
                            "min" => $min,
                            "max" => $max,
                            "custom" => $alarm instanceof SensorTypeCustom,
                            "description" => $this->getDescription($alarm, $max, $min),
                        ]);
                    }
                }
            });
            if (!$notes->isEmpty()) {
                $notes_page = view('reports.content.monthlyaveragenotes', compact('notes'))->render();
                PDF::AddPage();
                PDF::WriteHtml($notes_page, true, false, true, false, '');
            }
            $this->headerWritten = false;
        });
        PDF::LastPage();
        $report->setContent(PDF::Output($report->filename(), 'S'));
        return $report;
    }

    private function getSiteSensors(Site $site)
    {
        return Sensor::where('start_date', '<', $this->period->end())
            ->where('end_date', '>', $this->period->start())
            ->where('status', 'active')
            ->whereHas('device.site', function ($q) use ($site) {
                return $q->where('id', $site->id);
            })
            ->orderBy('name', 'asc')
            ->get();
    }

    private $headerWritten = false;
    private function writeHeader()
    {
        if (!$this->headerWritten) {
            PDF::SetFillColor(90, 236, 243);
            PDF::SetTextColor(0);
            PDF::SetDrawColor(129, 129, 129);
            PDF::SetLineWidth(0.3);
            PDF::SetFont('', 'B');
            PDF::Ln();
            $this->columns->each(function ($column, $index) {
                PDF::Cell($column["width"], 7, $column["header"], 1, 0, 'C', 1);
            });
            PDF::Ln();
            $this->headerWritten = true;
        }
    }

    private function writeRowStart($sensor, $location, $site, $alarm, $fill)
    {
        $should_fill = $fill;
        if (!is_null($alarm)) {
            PDF::SetFillColor($fill ? 224 : 253, 100, 100);
            if (!$fill) {
                $should_fill = true;
            }
        } else {
            PDF::SetFillColor(224, 235,255);
        }
        PDF::Cell($this->columns[0]["width"], 6, $sensor->id, 'LR', 0, $this->columns[0]["align"], $should_fill);
        PDF::Cell($this->columns[1]["width"], 6, $sensor->name, 'LR', 0, $this->columns[1]["align"], $should_fill);
        PDF::Cell($this->columns[2]["width"], 6, $location, 'LR', 0, $this->columns[2]["align"], $should_fill);
        PDF::Cell($this->columns[3]["width"], 6, $sensor->type_id, 'LR', 0, $this->columns[3]["align"], $should_fill);
        PDF::Cell($this->columns[4]["width"], 6, $site->id, 'LR', 0, $this->columns[4]["align"], $should_fill);
    }

    private function writeRowEnd($alarm, $min, $avg, $max, $fill)
    {
        if (!is_null($alarm)) {
            if ($alarm->type == "greater") {
                PDF::SetFillColor($fill ? 224 : 253, 100, 100);
                PDF::Cell($this->columns[5]["width"], 6, sprintf("%01.2f", $min), 'LR', 0, $this->columns[5]["align"], true);
                PDF::SetFillColor(224, 235,255);
                PDF::Cell($this->columns[6]["width"], 6, sprintf("%01.2f", $avg), 'LR', 0, $this->columns[6]["align"], $fill);
                PDF::Cell($this->columns[7]["width"], 6, sprintf("%01.2f", $max), 'LR', 0, $this->columns[7]["align"], $fill);
            } else {
                PDF::SetFillColor(224, 235,255);
                PDF::Cell($this->columns[5]["width"], 6, sprintf("%01.2f", $min), 'LR', 0, $this->columns[5]["align"], $fill);
                PDF::Cell($this->columns[6]["width"], 6, sprintf("%01.2f", $avg), 'LR', 0, $this->columns[6]["align"], $fill);
                PDF::SetFillColor($fill ? 224 : 253, 100, 100);
                PDF::Cell($this->columns[7]["width"], 6, sprintf("%01.2f", $max), 'LR', 0, $this->columns[7]["align"], true);
            }
        } else {
            PDF::SetFillColor(224, 235,255);
            PDF::Cell($this->columns[5]["width"], 6, sprintf("%01.2f", $min), 'LR', 0, $this->columns[5]["align"], $fill);
            PDF::Cell($this->columns[6]["width"], 6, sprintf("%01.2f", $avg), 'LR', 0, $this->columns[6]["align"], $fill);
            PDF::Cell($this->columns[7]["width"], 6, sprintf("%01.2f", $max), 'LR', 0, $this->columns[7]["align"], $fill);
        }
    }

    private function getDescription($alarm, $max, $min)
    {
        $greater = $alarm->type == "greater";
        return sprintf(
            "so the %s temperature must be %s than %s°C. During this month the %s temperature was %s°C",
            $greater ? "maximum" : "minimum",
            $greater ? "less" : "greater",
            $alarm->value,
            $greater ? "minimum" : "maximum",
            $greater ? $min : $max
        );
    }
}
