<?php

namespace App\Reports;

use Carbon\Carbon;

class ReportPeriod
{
    protected $period;

    public function __construct($period)
    {
        $this->period = $period;
    }

    public function start()
    {
        return Carbon::parse($this->period)->startOfMonth();
    }

    public function end()
    {
        return Carbon::parse($this->period)->endOfMonth();
    }

    public function endOrToday()
    {
        $today = Carbon::parse($this->period)->today()->endOfDay();
        $end = Carbon::parse($this->period)->endOfMonth();
        return $end > $today ? $today : $end;
    }
}
