<?php

namespace App\Reports;

use App\Customer;
use App\Reports\ReportInterface;
use App\Reports\Assets;
use App\RecentReport;
use App\Site;
use Auth;
use Carbon\Carbon;
use Storage;

class BaseReport implements ReportInterface
{
    protected $customer;
    protected $site;
    protected $filename;
    protected $filepath;

    public function __construct(Customer $customer = null, Site $site = null)
    {
        $this->customer = $customer;
        $this->site = $site;
    }

    // Does this report already exist in storage
    public function exists() : bool
    {
        return Storage::disk('rcmfiles')->exists($this->filepath);
    }


    public function url() : string
    {
        if (!$this->exists()) {
            return "";
        }
        return Storage::disk('rcmfiles')->temporaryUrl(
            $this->filepath,
            Carbon::now()->addMinutes(10)
        );
    }

    public function build(ReportBuilder $builder) : Report
    {
        return $builder->build($this->filename);
    }

    public function save(Report $report) : bool
    {
        if ($report->content() == '') {
            return false;
        }
        return Storage::disk('rcmfiles')->put($this->filepath, $report->content());
        // return Storage::put('test.pdf', $report->content());
    }
}
