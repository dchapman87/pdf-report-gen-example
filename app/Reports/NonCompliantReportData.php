<?php

namespace App\Reports;

use JpGraph\JpGraph;

JpGraph::load();
JpGraph::module('pie');

use PieGraph;
use UniversalTheme;
use PiePlot;
use App\Customer;
use App\Site;
use App\Sensor;
use App\Issue;

class NonCompliantReportData
{
    private $data;

    public function __construct(Customer $customer)
    {
        $this->data = collect([]);
        $this->data->push($this->createNonCompliantData($customer));
        foreach ($customer->sites as $site) {
            $this->data->push($this->createNonCompliantData($customer, $site));
        }
    }

    public function data()
    {
        return $this->data;
    }

    public function cleanup()
    {
        foreach($this->data as $data) {
            unlink($data->pieChartFilePath());
        }
    }

    private function createNonCompliantData(Customer $customer, Site $site = null)
    {
        return new class($customer, $site) {

            public $customer = null;
            public $site = null;

            private $_sensors = null;
            private $_disabledSensors = null;
            private $_issues = null;
            private $_pieChart = null;

            public function __construct(Customer $customer, Site $site = null)
            {
                $this->customer = $customer;
                if (!is_null($site)) {
                    $this->site = $site;
                }
                $this->generatePieChart();
            }

            public function sensors()
            {
                if (is_null($this->_sensors)) {
                    $this->_sensors = $this->getSensors();
                }
                return $this->_sensors;
            }

            public function disabledSensors()
            {
                if (is_null($this->_disabledSensors)) {
                    $this->_disabledSensors = $this->getSensors(true);
                }
                return $this->_disabledSensors;
            }

            public function compliantSensors()
            {
                return $this->sensors()->filter(function ($sensor, $key) {
                    return $sensor->compliant;
                });
            }

            public function nonCompliantSensors()
            {
                return $this->sensors()->filter(function ($sensor, $key) {
                    return !$sensor->compliant;
                });
            }

            public function issues()
            {
                if (is_null($this->_issues)) {
                    $this->_issues = $this->getIssues();
                }
                return $this->_issues;
            }

            public function nonCompliantIssues()
            {
                return $this->issues()->filter(function ($issue, $key) {
                    return $issue->status == "noncompliant";
                });
            }

            public function compliantIssues()
            {
                return $this->issues()->filter(function ($issue, $key) {
                    return $issue->status == "compliant";
                });
            }

            public function openIssues()
            {
                return $this->issues()->filter(function ($issue, $key) {
                    return $issue->action == "unsolved";
                });
            }

            public function openNonCompliantIssues()
            {
                return $this->openIssues()->filter(function ($issue, $key) {
                    return $issue->status == "noncompliant";
                });
            }

            public function closedIssues()
            {
                return $this->issues()->filter(function ($issue, $key) {
                    return $issue->action == "solved";
                });
            }

            public function pieChartFilePath()
            {
                return storage_path("/app/public/" . $this->pieChartFilename());
            }

            private function getSensors($disabled = false)
            {
                if (is_null($this->site)) {
                    $c = $this->customer;
                    return Sensor::whereHas('device.site', function ($q) use ($c) {
                        return $q->where('customer_id', $c->id);
                    })->where('status', $disabled ? 'inactive' : 'active')->get();
                } else {
                    $s = $this->site;
                    return Sensor::whereHas('device.site', function ($q) use ($s) {
                        return $q->where('id', $s->id);
                    })->where('status', $disabled ? 'inactive' : 'active')->get();
                }
            }

            private function getIssues()
            {
                if (is_null($this->site)) {
                    $c = $this->customer;
                    return Issue::whereHas('sensor.device.site', function ($q) use ($c) {
                        return $q->where('customer_id', $c->id);
                    })->get();
                } else {
                    $s = $this->site;
                    return Issue::whereHas('sensor.device.site', function ($q) use ($s) {
                        return $q->where('id', $s->id);
                    })->get();
                }
            }

            private function generatePieChart()
            {
                $graph = $this->createPieGraphInstance();
                $gdImgHandler = $graph->Stroke(_IMG_HANDLER);
                $graph->img->Stream($this->pieChartFilePath());
            }

            private function createPieGraphInstance()
            {
                $pieGraph = new PieGraph(430,430);
                $pieGraph->title->Set("Event Chart of Organisation");
                $pieGraph->SetBox(true);

                $plot = new PiePlot([
                    $this->compliantSensors()->count(),
                    $this->nonCompliantSensors()->count()
                ]);
                $plot->SetSize(0.35);
                $plot->SetCenter(0.35,0.34);
                $plot->value->SetFont(FF_FONT1, FS_BOLD);

                $pieGraph->Add($plot);

                $plot->ShowBorder();
                $plot->SetColor('black');
                $plot->SetSliceColors(['#73B67B','#F8021F']);
                return $pieGraph;
            }

            private function pieChartFilename()
            {
                return is_null($this->site) ? sprintf(
                    "%d_cspiechart.jpg",
                    $this->customer->id
                ) : sprintf(
                    "%d_%d_cspiechart.jpg",
                    $this->customer->id,
                    $this->site->id
                );
            }
        };
    }
}