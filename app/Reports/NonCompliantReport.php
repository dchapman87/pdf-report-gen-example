<?php

namespace App\Reports;

use App\Reports\ReportInterface;

class NonCompliantReport implements ReportInterface
{

    public $title = "Report 5 - Outstanding Non-Compliant Report";
    public $filename = null;

    public function __construct()
    {
    }

    // Does this report already exist in storage
    public function exists()
    {
        return false;
    }
    // Render new report and store it in storage
    public function render()
    {
        return view('reports.noncompliant')->render();
    }
    // Fetch stored report
    public function fetch()
    {

    }
}