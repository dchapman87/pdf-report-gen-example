<?php

namespace App\Reports;

use App\Reports\ReportInterface;

interface ReportBuilder
{
    public function isValid() : bool;
    public function build(string $filename) : Report;
}
