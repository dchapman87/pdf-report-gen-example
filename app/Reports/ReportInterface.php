<?php

namespace App\Reports;

use App\Reports\ReportBuilder;

interface ReportInterface
{
    public function exists() : bool;
    public function url() : string;
    public function build(ReportBuilder $builder) : Report;
    public function save(Report $report) : bool;
}

// $assets = new AssetsReport($site);
// if ($assets->exists()) {
//     return $assets->url();
// }

// $builder = new AssetsBuilder();
// $report = $assets->build(new AssetsBuilder());
// if ($report->isValid()) {
//     $assets->save($report);
// }
