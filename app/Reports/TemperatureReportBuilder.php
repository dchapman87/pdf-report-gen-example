<?php
namespace App\Reports;

use App\Customer;
use App\Site;
use App\Sensor;
use PDF;
use Carbon\Carbon;

class TemperatureReportBuilder implements ReportBuilder
{
    private $site;
    private $customer;
    private $period;
    private $sites;

    public function __construct(Customer $customer = null, Site $site = null, ReportPeriod $period = null)
    {
        if (!is_null($site)) {
            $this->site = $site;
            if (is_null($customer)) {
                $this->customer = $site->customer;
            }
        }

        if (!is_null($customer)) {
            $this->customer = $customer;
        }

        if (is_null($site)) {
            $this->sites = $customer->sites;
        } else {
            $this->sites = collect([$site]);
        }

        $this->period = $period;
    }

    public function isValid(): bool
    {
        return !is_null($this->customer);
    }

    public function build(string $filename) : Report
    {
        $report = new Report();
        $report->setFilename($filename);
        PDF::reset();
        PDF::SetCreator(PDF_CREATOR);
        PDF::SetAuthor(PDF_AUTHOR);
        PDF::SetTitle('Report 2 - Temperature Report');
        PDF::SetSubject('');
        PDF::SetKeywords('');
        // TODO: Figure out why the logo isn't being inserted???
        PDF::SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', '');
        // set margins
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(PDF_MARGIN_HEADER);
        PDF::SetFooterMargin(PDF_MARGIN_FOOTER);

        // set header and footer fonts
        // PDF::SetHeaderFont(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN);
        // PDF::SetFooterFont(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA);
        PDF::SetFont('helvetica', '', 9);
        PDF::SetAutoPageBreak(true, 10);
        PDF::SetTextColor(0, 0, 0);
        $customer = $this->customer;
        $period = $this->period;
        $this->sites->each(function ($site, $index) use ($customer, $period) {
            $sensors = $this->getSiteSensors($site);
            foreach ($sensors as $sensor) {
                PDF::AddPage();
                $data = new TemperatureReportData($customer, $site, $sensor, $period);
                $page = view('reports.content.temperature', compact(['data']))->render();
                PDF::WriteHtml($page, true, false, true, false, '');
                if (!$data->tableData->isEmpty()) {
                    if ($data->tableData->count() > 9) {
                        PDF::AddPage();
                    }
                    PDF::SetMargins(60, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
                    $headers = collect(['Date', 'Min','Avg','Max']);
                    $col_width = [30, 20, 20, 20];
                    $align = ['L', 'C', 'C', 'C'];
                    PDF::SetFillColor(90, 236, 243);
                    PDF::SetTextColor(0);
                    PDF::SetDrawColor(129, 129, 129);
                    PDF::SetLineWidth(0.3);
                    PDF::SetFont('', 'B');
                    PDF::Ln();
                    $headers->each(function ($header, $index) use ($col_width) {
                        PDF::Cell($col_width[$index], 7, $header, 1, 0, 'C', 1);
                    });
                    PDF::Ln();
                    PDF::SetFillColor(224, 235, 255);
                    PDF::SetFont('');
                    $data->tableData->each(function ($row, $index) use ($col_width, $align) {
                        // we want every other row to be colored but the first one white
                        $fill = $index % 2 != 0;
                        PDF::Cell($col_width[0], 6, Carbon::createFromTimestamp($row[0])->toDateString(), 'LR', 0, $align[0], $fill);
                        PDF::Cell($col_width[1], 6, $row[1], 'LR', 0, $align[1], $fill);
                        PDF::Cell($col_width[2], 6, $row[2], 'LR', 0, $align[2], $fill);
                        PDF::Cell($col_width[3], 6, $row[3], 'LR', 0, $align[3], $fill);
                        PDF::Ln();
                    });
                    PDF::Cell(array_sum($col_width), 0, '', 'T');
                    PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
                }
                $data->cleanUp();
            }
        });

        PDF::LastPage();
        $report->setContent(PDF::Output($report->filename(), 'S'));
        return $report;
    }

    private function getSiteSensors(Site $site)
    {
        return Sensor::where('start_date', '<', $this->period->end())
            ->where('end_date', '>', $this->period->start())
            ->where('status', 'active')
            ->whereHas('device.site', function ($q) use ($site) {
                return $q->where('id', $site->id);
            })
            ->orderBy('name', 'asc')
            ->get();
    }
}
