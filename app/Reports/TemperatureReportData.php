<?php

namespace App\Reports;

use App\Customer;
use App\Site;
use App\Sensor;
use Carbon\Carbon;
use JpGraph\JpGraph;
use Storage;

JpGraph::load();
JpGraph::module("line");

use Graph;
use UniversalTheme;
use LinePlot;
use App\SensorLog;
use App\Issue;

class TemperatureReportData
{
    public $customer;
    public $site;
    public $sensor;
    public $period;
    public $tableData;
    public $imageUrl;
    private $alarm_high = 3;
    private $alarm_low = 0;

    protected $graphData;

    public function __construct(Customer $customer, Site $site, Sensor $sensor, ReportPeriod $period)
    {
        $this->customer = $customer;
        $this->site = $site;
        $this->sensor = $sensor;
        $this->period = $period;
        $this->graphData = [
            'max' => collect([]),
            'average' => collect([]),
            'min' => collect([]),
            'timestamps' => collect([]),
            'alarms' => collect([]),
            'alarms_timestamps' => collect([]),
            'events' => collect([]),
            'events_timestamps' => collect([]),
        ];
        $this->initialEvent = [
            'event' => null,
            'timestamp' => 0
        ];
        $this->tableData = collect([]);
        $this->buildData();
        $this->generateImage();
    }

    private function generateImage()
    {
        $graph = $this->createGraphInstance();
        $gdImgHandler = $graph->Stroke(_IMG_HANDLER);
        $fileName = "line".rand()."graph.jpg";
        $filePath = storage_path("/app/public/" . $fileName);
        $graph->img->Stream($filePath);
        $this->imageUrl = $filePath;
        // get list of sensor logs for period
        // if empty return empty url
        //
    }

    private function createGraphInstance()
    {
        $graph = new Graph(940, 350, 'auto');
        $graph->SetScale(
            'linlin',
            0,
            0,
            $this->period->start()->timestamp,
            $this->period->end()->timestamp
        );
        $graph->SetY2Scale('lin', 0, 100);
        $graph->SetTheme(new UniversalTheme());

        $graph->img->SetAntiAliasing(false);
        $graph->title->Set(sprintf("Temperature Report Graph\n%s", $this->sensor->name));
        $graph->SetBox(false);
        $graph->img->SetAntiAliasing();

        $graph->SetMargin(40, 30, 40, 44);
        $graph->legend->SetAbsPos(30, 10, 'right', 'top');
        $graph->legend->SetFrameWeight(1);

        $graph->xaxis->SetLabelFormatString('d M', true);  //Set X axis label
        $graph->xaxis->SetLabelAngle(90); //Angle them 90 deg
        $graph->xaxis->SetLabelMargin(4);
        $graph->xaxis->SetTitle(sprintf("Date (Year %s)", $this->period->start()->year), 'middle');
        $graph->xaxis->SetTitleMargin(37);

        $graph->yaxis->SetTitle('Temperature [Deg C]', 'middle');
        $graph->yaxis->HideZeroLabel();
        $graph->yaxis->HideLine(false);
        $graph->yaxis->HideTicks(false, false);

        $graph->xgrid->Show();
        $graph->xgrid->SetLineStyle("solid");
        $graph->xgrid->SetColor('#E3E3E3');

        $p1 = new LinePlot(
            $this->graphData['max']->toArray(),
            $this->graphData['timestamps']->toArray()
        );
        $p1->SetColor('#6495ED');
        $p1->SetLegend('Max');
        $graph->Add($p1);

        // Create the second line
        $p2 = new LinePlot(
            $this->graphData['average']->toArray(),
            $this->graphData['timestamps']->toArray()
        );
        $p2->SetColor('#B22222');
        $p2->SetLegend('Avg');
        $graph->Add($p2);

        // Create the third line
        $p3 = new LinePlot(
            $this->graphData['min']->toArray(),
            $this->graphData['timestamps']->toArray()
        );
        $p3->SetColor('#FF1493');
        $p3->SetLegend('Min');
        $graph->Add($p3);

        $pAlarm_amba = new LinePlot(
            $this->graphData['alarms']->toArray(),
            $this->graphData['alarms_timestamps']->toArray()
        );
        $pAlarm_amba->SetColor("yellow");
        $pAlarm_amba->SetFillColor("yellow");
        $graph->AddY2($pAlarm_amba);

        $pAlarm_red = new LinePlot(
            $this->graphData['events']->toArray(),
            $this->graphData['events_timestamps']->toArray()
        );
        $pAlarm_red->SetColor("red");
        $pAlarm_red->SetFillColor("red");
        $graph->AddY2($pAlarm_red);
        return $graph;
    }

    private function buildData()
    {
        $result = SensorLog::where('sensor_id', $this->sensor->id)
            ->where('time', '>=', $this->period->start())
            ->where('time', '<=', $this->period->end())
            ->dailyAggregate();

        if ($result->isEmpty()) {
            $this->graphData['max']->push(0); // datay1
            $this->graphData['average']->push(0); // datay2
            $this->graphData['min']->push(0); //datay3
            $this->graphData['timestamps']->push($this->period->start()->timestamp);
            $this->graphData['events']->push($this->alarm_low);
            $this->graphData['events_timestamps']->push($this->period->start()->timestamp);
        }
        $this->graphData['alarms']->push($this->alarm_low);
        $this->graphData['alarms_timestamps']->push($this->period->start()->timestamp);
        $inAlarm = false;
        $result->each(function ($log, $index) use ($inAlarm) {
            $timestamp = Carbon::parse($log['time'])->timestamp;
            // Graph Data
            $this->graphData['max']->push(sprintf("%01.2f", $log['max'])); // datay1
            $this->graphData['average']->push(sprintf("%01.2f", $log['avg'])); // datay2
            $this->graphData['min']->push(sprintf("%01.2f", $log['min'])); //datay3
            $this->graphData['timestamps']->push($timestamp); // newdt
            if ($this->sensor->inAlarmFromLog($log)  && $this->graphData['alarms']->last() == $this->alarm_low) {
                $this->graphData['alarms']->push($this->alarm_low);
                $this->graphData['alarms_timestamps']->push($timestamp);
                $this->graphData['alarms']->push($this->alarm_high);
                $this->graphData['alarms_timestamps']->push($timestamp);
                $inAlarm = true;
            } elseif ($inAlarm && $this->graphData['alarms']->last() == $this->alarm_high) {
                $this->graphData['alarms']->push($this->alarm_high);
                $this->graphData['alarms_timestamps']->push($timestamp);
                $this->graphData['alarms']->push($this->alarm_low);
                $this->graphData['alarms_timestamps']->push($timestamp);
                $inAlarm = false;
            }


            $events = Issue::with('audits')->where('sensor_id', $this->sensor->id)
                ->where('created_at', '>=', $this->period->start())
                ->where('created_at', '<=', $this->period->end())
                ->get();

            if ($events->isEmpty()) {
                $this->graphData['events']->push($this->alarm_low);
                $this->graphData['events_timestamps']->push($this->period->start()->timestamp);
            } else {
                // else loop over events and find the initial state and update height based on status changes
                $events->each(function ($event, $index) {
                    $eventtime = Carbon::parse($event->created_at)->timestamp;
                    if ($event->audits->isEmpty()) {
                        $compliant = $event->status == "compliant";
                        // No changes to the alarm
                        $this->graphData['events']->push($this->alarm_low);
                        $this->graphData['events_timestamps']->prepend($eventtime);
                        if (!$compliant) {
                            $this->graphData['events']->push($this->alarm_high);
                            $this->graphData['events_timestamps']->prepend($eventtime);
                            $this->graphData['events']->push($this->graphData['events']->last());
                            $this->graphData['events_timestamps']->prepend($this->period->endOrToday()->timestamp);
                        }
                    } else {
                        $this->initialEvent['timestamp'] = $eventtime;
                        // loop changes and determine the initial state while processing
                        $event->audits->each(function ($update, $index) {
                            $updatetime = Carbon::parse($update->created_at)->timestamp;
                            $newValues = $update->new_values;
                            if (array_key_exists("status", $newValues)) {
                                $compliant = $newValues["status"] == "compliant";
                                if ($compliant && $this->graphData['events']->last() == $this->alarm_high) {
                                    $this->graphData['events']->push($this->alarm_high);
                                    $this->graphData['events_timestamps']->push($updatetime);
                                    $this->graphData['events']->push($this->alarm_low);
                                    $this->graphData['events_timestamps']->push($updatetime);
                                } elseif (!$compliant && $this->graphData['events']->last() == $this->alarm_low) {
                                    $this->graphData['events']->push($this->alarm_low);
                                    $this->graphData['events_timestamps']->push($updatetime);
                                    $this->graphData['events']->push($this->alarm_high);
                                    $this->graphData['events_timestamps']->push($updatetime);
                                }

                                if (is_null($this->initialEvent["event"])) {
                                    $this->initialEvent["event"] = $newValues["status"] == "noncompliant" ? $this->alarm_low : $this->alarm_high;
                                }
                            }
                        });

                        if (is_null($this->initialEvent["event"])) {
                            // ok there were no changes to the status so go with create_at to period end
                            $this->initialEvent["event"] = $event->status == "noncompliant" ? $this->alarm_high : $this->alarm_low;
                            $this->graphData['events']->push($this->initialEvent["event"]);
                            $this->graphData['events_timestamps']->push($this->period->endOrToday()->timestamp);
                        }
                    }
                });

                if (!is_null($this->initialEvent["event"])) {
                    $this->graphData['events']->prepend($this->initialEvent["event"]);
                    $this->graphData['events_timestamps']->prepend($this->initialEvent["timestamp"]);
                    $this->graphData['events']->prepend($this->alarm_low);
                    $this->graphData['events_timestamps']->prepend($this->initialEvent["timestamp"]);
                }

                $this->graphData['events']->prepend($this->alarm_low);
                $this->graphData['events_timestamps']->prepend($this->period->start()->timestamp);
            }

            // Table Data
            $this->tableData->push([
                $timestamp,
                sprintf("%01.2f", $log['min']),
                sprintf("%01.2f", $log['avg']),
                sprintf("%01.2f", $log['max']),
            ]);
            // dd($this->graphData);
        });
    }

    public function cleanUp()
    {
        unlink($this->imageUrl);
    }
}
