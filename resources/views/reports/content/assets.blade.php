<div>
    @foreach ($assets->sites() as $site)
        <div  class="ibox-content">
            <table  width="100%" cellpadding="1" cellspacing="1">
                <tr>
                    <td align="right">
                        <h2 style="color:#000;">
                            <font color="#000">
                                Date: {{ date('d/m/Y') }}
                            </font>
                        </h2>
                    </td>
                </tr>
            </table>
            <hr style="height:2px;"/>
                <table width="100%" cellpadding="1" cellspacing="1">
                    <tr>
                        <td align="center">
                            <h2 style="color:#000;">
                                <font color="#05A7AE">
                                    Site/Organisation Assets
                                </font>
                            </h2>
                        </td>
                    </tr>
                </table>
                <hr style="height:2px;"/>
                <table width="100%" cellpadding="2" cellspacing="2" border="0">
                    <tr>
                        <td width="49%">
                            <h2 style="background:#E8F6F6; color:#000;">
                                Organisation Name:
                                <font color="#05A7AE">
                                    {{ $assets->customer()->name }}
                                </font>
                            </h2>
                        </td>
                        <td width="1%">&nbsp;</td>
                        <td width="49%">
                            <h2 style="background:#E8F6F6; color:#000;">
                                Address:
                                <font color="#05A7AE">
                                    {{ $assets->customer()->address->address_1 }},
                                    @if ($assets->customer()->address->address_2)
                                        {{ $assets->customer()->address->address_2 }},
                                    @endif
                                    @if ($assets->customer()->address->town)
                                        {{ $assets->customer()->address->town }},
                                    @endif
                                    @if ($assets->customer()->address->county)
                                        {{ $assets->customer()->address->county }},
                                    @endif
                                    {{ $assets->customer()->address->post_code }}
                                </font>
                            </h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" cellpadding="2" cellspacing="2">
                                <tr>
                                    <td width="75%">Total  Number of Sites</td>
                                    <td width="5%">&nbsp;</td>
                                    <td width="15%"> {{ $assets->customer()->sites->count() }}</td>
                                </tr>
                                <tr>
                                    <td>Total  Number of Devices</td>
                                    <td>&nbsp;</td>
                                    <td> {{ $assets->devices()->count() }}</td>
                                </tr>
                                <tr>
                                    <td>Total  Number of Sensors</td>
                                    <td>&nbsp;</td>
                                    <td>{{ $assets->sensors()->count() }}</td>
                                </tr>
                                <tr>
                                    <td>Total  Number of Outputs</td>
                                    <td>&nbsp;</td>
                                    <td>0</td>
                                </tr>
                            </table>
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                <hr style="height:2px;background-color:#05A7AE;"/>
                <table width="100%" cellpadding="2" cellspacing="2">
                    <tr>
                        <td width="60%">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <h4 style="background:#E8F6F6; color:#000;">
                                            Site name :
                                            <font color="#05A7AE">
                                                {{ $site->name }}
                                            </font>
                                        </h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td><h4 style="background:#E8F6F6; color:#000;">
                                        Address :
                                    {{ $site->address->address_1 }},
                                    @if ($site->address->address_2)
                                        {{ $site->address->address_2 }},
                                    @endif
                                    @if ($site->address->town)
                                        {{ $site->address->town }},
                                    @endif
                                    @if ($site->address->county)
                                        {{ $site->address->county }},
                                    @endif
                                    {{ $site->address->post_code }}</h4></td>
                                </tr>
                            </table>
                        </td>
                        <td width="2%">&nbsp;</td>
                        <td width="38%" valign="top">
                            <table width="100%" cellpadding="2" cellspacing="2">
                                <tr>
                                    <td>
                                        Total Number Of Devices : {{ $site->devices->count() }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Total Number Of Sensors : {{ $assets->siteSensors($site->id)->count() }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Total Number Of Outputs : 0
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                @if ($site->devices->isNotEmpty())
                @foreach ($site->devices as $device)
                    <hr style="height:2px;background-color:#05A7AE;" />
                    <table width="100%" cellpadding="2" cellspacing="2">
                        <tr>
                            <td width="10%">&nbsp;</td>
                            <td width="90%"><h4 style="color:#000;">Device : <font color="#05A7AE">{{ $device->name }}</font></h4> </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td><h4 style="color:#000;">location :  {{ $device->location->title }}</h4></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td valign="top">
                            <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #red;margin: 1em 0; padding: 0;" />
                            <table width="100%" cellpadding="2" cellspacing="2">
                                <tr>
                                    <td width="21%"><h4 style="background:#E8F6F6; color:#000;">Sensor Name</h4></td>
                                    <td width="21%"><h4 style="background:#E8F6F6; color:#000;">Serial Number</h4></td>
                                    <td width="58%"><h4 style="background:#E8F6F6; color:#000;">Sensor Location</h4></td>
                                </tr>
                                @if ($device->sensors->isNotEmpty())
                                @foreach ($device->sensors as $sensor)
                                    <tr>
                                        <td>{{ $sensor->name }}</td>
                                        <td>{{ $sensor->serial }}</td>
                                        <td>{{ $sensor->location->title }}</td>
                                    </tr>
                                @endforeach
                                @else
                                    <tr>
                                        <td colspan="2" align="center">No Sensors Found</td>
                                    </tr>
                                @endif
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td valign="top">
                            <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #red;margin: 1em 0; padding: 0;" />
                            <table width="100%" cellpadding="2" cellspacing="2">
                                <tr>
                                    <td width="21%"><h4 style="background:#E8F6F6; color:#000;">Output Name</h4></td>
                                    <td width="21%"><h4 style="background:#E8F6F6; color:#000;">Channel Number</h4></td>
                                    <td width="58%"><h4 style="background:#E8F6F6; color:#000;">Output Location</h4></td>
                                </tr>
                                <!-- if ($device->outputs->isNotEmpty()) -->
                                @if (false)
                                @foreach ($device->outputs as $outputs)
                                    <tr>
                                        <td>{{ $outputs->name }}</td>
                                        <td>{{ $outputs->serial }}</td>
                                        <td>{{ $outputs->location->title }}</td>
                                    </tr>
                                @endforeach
                                @else
                                    <tr>
                                        <td colspan="2" align="center">No Outputs Found</td>
                                    </tr>
                                @endif
                            </table>
                        </td>
                    </tr>
                </table>
            @endforeach
            @else
                <table width="100%" cellpadding="2" cellspacing="2">
                    <tr>
                        <td colspan="2" align="center">No Devices Found</td>
                    </tr>
                </table>
            @endif
        </div>
        @if ($site != $assets->sites()->last())
        <tcpdf method="AddPage" />
        @endif
    @endforeach
</div>