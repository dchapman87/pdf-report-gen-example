@extends('reports.page')

@section('content')
<table width="100%" border="0" cellpadding="4" cellspacing="4">
    <tr>
        <td align="center">
            <h2 style="background:#E8F6F6; color:#000;">
                <font color="#05A7AE">Temperature Monthly Average Report</font>
            </h2>
        </td>
    </tr>
</table>
<hr style="height:1px;background-color:#05A7AE;"/>
<table width="100%" border="0" cellpadding="2" cellspacing="2">
    <tr>
        <td width="45%">Organisation</td>
        <td width="3%">:</td>
        <td align="left" width="52%">
            <font color="#05A7AE">{{ $customer->name }}</font>
        </td>
    </tr>
    <tr>
        <td>Site name</td>
        <td>:</td>
        <td align="left">
            <font color="#05A7AE">{{ $site->name }}</font>
        </td>
    </tr>
    <tr>
        <td>Date From </td>
        <td>:</td>
        <td align="left">
            <font color="#05A7AE">{{ $period->start()->toDateString() }}</font>
        </td>
    </tr>
    <tr>
        <td>Date Finish </td>
        <td>:</td>
        <td align="left">
            <font color="#05A7AE">{{ $period->endOrToday()->toDateString() }}</font>
        </td>
    </tr>
    <tr>
        <td>Reporting Month</td>
        <td>:</td>
        <td align="left">
            <font color="#05A7AE">{{ $period->start()->format("F Y") }}</font>
        </td>
    </tr>
    <tr>
        <td>Number of sensors</td>
        <td>:</td>
        <td align="left">
            <font color="#05A7AE">{{ $data->count() }}</font>
        </td>
    </tr>
</table>
<br>
<hr style="height:1px;" color="#05A7AE"/>
@endsection
