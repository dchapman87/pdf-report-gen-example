<h2 style="background:#E8F6F6; color:#000;"><font color="#000">Notes:</font></h2>
<hr style="height:1px;background-color:#05A7AE;"/>
<ul>
@foreach ($notes as $note)
    @if ($note["custom"])
    <li><b>{{ $note["sensor"]["id"] }}</b> Sensor {{ $note["sensor"]["name"] }} - {{ $note["location"] }} is a custom alarm with description - {{ $note["alarm"]["description"] }} {{ $note["description"] }}</li>
    @else
    <li><b>{{ $note["sensor"]["id"] }}</b> Sensor {{ $note["sensor"]["name"] }} - {{ $note["location"] }} is a {{ $note["sensor"]["type"]["description"] }} {{ $note["description"] }}</li>
    @endif
@endforeach
</ul>