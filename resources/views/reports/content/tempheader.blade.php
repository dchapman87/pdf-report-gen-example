<table width="100%" border="0" cellpadding="4" cellspacing="4">
    <tr>
	    <td align="center"><h2 style="background:#E8F6F6; color:#000;"><font color="#05A7AE">Temperature Report</font></h2></td>
    </tr>
</table>
<hr style="height:2px;background-color:#05A7AE;"/>
<table width="100%" border="0" cellpadding="2" cellspacing="2">
    <tr>
        <td width="45%">Organisation</td>
		<td width="3%">:</td>
		<td align="left" width="52%"><font color="#05A7AE">{{ $data->customer->name }}</font></td>
    </tr>
    <tr>
        <td>Site name</td>
		<td>:</td>
		<td align="left"><font color="#05A7AE">{{ $data->site->name }}</font></td>
    </tr>
    <tr>
		<td>Address </td>
		<td>:</td>
		<td align="left"><font color="#05A7AE">
                {{ $data->site->address->address_1 }},
                @if ($data->site->address->address_2)
                    {{ $data->site->address->address_2 }},
                @endif
                @if ($data->site->address->town)
                    {{ $data->site->address->town }},
                @endif
                @if ($data->site->address->county)
                    {{ $data->site->address->county }},
                @endif
                {{ $data->site->address->post_code }}
        </font></td>
    </tr>
    <tr>
        <td>Reporting Month</td>
		<td>:</td>
		<td align="left"><font color="#05A7AE">{{ $data->period->start()->format('Y-m') }}</font></td>
    </tr>
</table>
<hr/>
<table width="100%" border="0" cellpadding="2" cellspacing="2">
<tr>
    <td>Sensor :</td>
	<td>{{ $data->sensor ? $data->sensor->name : "No Name" }}</td>
	<td>Location:</td>
	<td>{{ $data->sensor->location ? $data->sensor->location->title : "Unknown location" }}</td>
</tr>
<tr>
    <td>Sensor type:</td>
	<td>{{ $data->sensor->type->name }}</td>
	<td>Service due:</td>
	<td>{{ $data->sensor->service_due }}</td>
</tr>
<tr>
	<td colspan="4"><hr style="height:2px;" color="#05A7AE"/></td>
</tr>
<tr>
	<td colspan="4">
    	<hr style="height:2px;" color="#05A7AE"/>
	</td>
</tr>