@extends('reports.page')

@section('content')
<div>
    @component('reports.content.tempheader', ['data' => $data])
    @endcomponent
    <tr>
        <td align="center" colspan="4">
            <img src="{{ $data->imageUrl }}" />
            <br>
            <b> {{ $data->period->start()->format('F Y') }}</b>
            <br>
        </td>
    </tr>
</div>


@endsection
